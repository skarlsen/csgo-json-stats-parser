# CSGO JSON Stats Parser #

This is an application I created during MLG Columbus 2016 for the CSGO Fantasy Team Game. The application calculates how many points each players earned in each of the five possible categories for the selected stage of the tournament. Based on this, the application creates a file containing sorted tables for each category, presenting the best picks in these categories. The file also contains the best possible five-player lineup where a player isn't selected for more than one role. 

### Contains ###

* Python script that parses valid JSON and presents the aforementioned statistics and tables based on the data contained within.  
* PHP script that converts the player statistics patched into the game by Valve in their Valve Data Format (.vdf) to valid JSON  
* JSON data and calculated points for each stage of the MLG Columbus 2016 Major Tournament  