# CSGO JSON Data Parser
# Version 0.6
# Calculates optimal picks for fantasy teams for specified day
# Requires properly formatted JSON data

import json
import itertools
import pprint
import collections
from operator import itemgetter
from prettytable import PrettyTable

commando = []
avg_commando = []
clutch_king = []
avg_clutch_king = []
eco_warrior = []
avg_eco_warrior = []
entry_fragger = []
avg_entry_fragger = []
sniper = []
avg_sniper = []
optimal_candidates = []
optimal_combinations = []

stageToParse = "stage6"
input_file = "MLG2016data.json"       # Download: http://hastebin.com/raw/ukexolikos
output_file = "MLG2016finalPoints.txt"

def calculateDailyCommando (name, kills, deaths):
    global commando
    points = 2*kills
    points -= deaths
    plusminus = kills-deaths
    if plusminus > 0:
        points += plusminus
    commando.append([name, points, plusminus])

def calculateAvgCommando (name, kills, deaths, matches):
    global avg_commando
    points = 2*kills
    points -= deaths
    plusminus = kills-deaths
    if plusminus > 0:
        points += plusminus
    avg_commando.append([name, float(points/matches), float(plusminus/matches), matches])

def calculateDailyClutchKing (name, kills, deaths, clutch_kills):
    global clutch_king
    points = 2*kills
    points -= deaths
    points += 4*clutch_kills
    clutch_king.append([name, points, clutch_kills])

def calculateAvgClutchKing (name, kills, deaths, clutch_kills, matches):
    global avg_clutch_king
    points = 2*kills
    points -= deaths
    points += 4*clutch_kills
    avg_clutch_king.append([name, float(points/matches), float(clutch_kills/matches), matches])

def calculateDailyEcoWarrior (name, kills, deaths, pistol_kills):
    global eco_warrior
    points = 2*kills
    points -= deaths
    points += 2*pistol_kills
    eco_warrior.append([name, points, pistol_kills])

def calculateAvgEcoWarrior (name, kills, deaths, pistol_kills, matches):
    global avg_eco_warrior
    points = 2*kills
    points -= deaths
    points += 2*pistol_kills
    avg_eco_warrior.append([name, float(points/matches), float(pistol_kills/matches), matches])

def calculateDailyEntryFragger (name, kills, deaths, opening_kills):
    global entry_fragger
    points = 2*kills
    points -= deaths
    points += 2*opening_kills
    entry_fragger.append([name, points, opening_kills])

def calculateAvgEntryFragger (name, kills, deaths, opening_kills, matches):
    global avg_entry_fragger
    points = 2*kills
    points -= deaths
    points += 2*opening_kills
    avg_entry_fragger.append([name, float(points/matches), float(opening_kills/matches), matches])

def calculateDailySniper (name, kills, deaths, sniper_kills):
    global sniper
    points = 2*kills
    points -= deaths
    points += 2*sniper_kills
    sniper.append([name, points, sniper_kills])

def calculateAvgSniper (name, kills, deaths, sniper_kills, matches):
    global avg_sniper
    points = 2*kills
    points -= deaths
    points += 2*sniper_kills
    avg_sniper.append([name, float(points/matches), float(sniper_kills/matches), matches])

def findOptimal():
    global s_commando, s_clutch_king, s_eco_warrior, s_entry_fragger, s_sniper, optimal_candidates, optimal_combinations
    for a in range (0,5):
        for b in range (0,5):
            for c in range (0,5):
                for d in range (0,5):
                    for e in range(0,5):
                        optimal_candidates.append(
                            [s_commando[a][0], s_clutch_king[b][0], s_eco_warrior[c][0], s_entry_fragger[d][0], s_sniper[e][0],
                            s_commando[a][1] + s_clutch_king[b][1] + s_eco_warrior[c][1] + s_entry_fragger[d][1] + s_sniper[e][1]]
                            )


    for candidate in optimal_candidates:
        unique_values = set(candidate)
        if len(unique_values) == 6:
            optimal_combinations.append(candidate)

def printOptimal (sortedList, outFile):
    outFile.write('Best lineups today:\n')
    optimal = PrettyTable(["Commando", "Clutch King", "Eco Warrior", "Entry Fragger", "Sniper", "Total Points"])
    optimal.align = "l"
    optimal.align["Total Points"] = "r"
    for entry in sortedList:
        optimal.add_row(entry)
    str_optimal = optimal.get_string()
    outFile.write(str_optimal)

def printCommando (sortedList, outFile):
    outFile.write('\n\nDaily Commando: \n')
    commando = PrettyTable(["Name", "Points", "Kills - Deaths"])
    commando.align["Name"] = "l"
    commando.align["Points"] = "r"
    commando.align["Kills - Deaths"] = "r"
    for entry in sortedList:
        commando.add_row(entry)
    str_commando = commando.get_string()
    outFile.write(str_commando)

def printAvgCommando (sortedList, outFile):
    outFile.write('\n\nCommando Total Average: \n')
    avg_commando = PrettyTable(["Name", "Points/Maps", "Kills - Deaths/Maps", "Maps Played"])
    avg_commando.float_format = ".2"
    avg_commando.align = "r"
    avg_commando.align["Name"] = "l"
    for entry in sortedList:
        avg_commando.add_row(entry)
    str_avg_commando = avg_commando.get_string()
    outFile.write(str_avg_commando)

def printClutchKing (sortedList, outFile):
    outFile.write('\n\nDaily Clutch King: \n')
    clutch_king = PrettyTable(["Name", "Points", "Clutch Kills"])
    clutch_king.align["Name"] = "l"
    clutch_king.align["Points"] = "r"
    clutch_king.align["Clutch Kills"] = "r"
    for entry in sortedList:
        clutch_king.add_row(entry)
    str_clutch_king = clutch_king.get_string()
    outFile.write(str_clutch_king)

def printAvgClutchKing (sortedList, outFile):
    outFile.write('\n\nClutch King Total Average: \n')
    avg_clutch_king = PrettyTable(["Name", "Points/Maps", "Clutch Kills/Maps", "Maps Played"])
    avg_clutch_king.float_format = ".2"
    avg_clutch_king.align = "r"
    avg_clutch_king.align["Name"] = "l"
    for entry in sortedList:
        avg_clutch_king.add_row(entry)
    str_avg_clutch_king = avg_clutch_king.get_string()
    outFile.write(str_avg_clutch_king)

def printEcoWarrior (sortedList, outFile):
    outFile.write('\n\nDaily Eco Warrior: \n')
    eco_warrior = PrettyTable(["Name", "Points", "Pistol Kills"])
    eco_warrior.align["Name"] = "l"
    eco_warrior.align["Points"] = "r"
    eco_warrior.align["Pistol Kills"] = "r"
    for entry in sortedList:
        eco_warrior.add_row(entry)
    str_eco_warrior = eco_warrior.get_string()
    outFile.write(str_eco_warrior)

def printAvgEcoWarrior (sortedList, outFile):
    outFile.write('\n\nEco Warrior Total Average: \n')
    avg_eco_warrior = PrettyTable(["Name", "Points/Maps", "Pistol Kills/Maps", "Maps Played"])
    avg_eco_warrior.float_format = ".2"
    avg_eco_warrior.align = "r"
    avg_eco_warrior.align["Name"] = "l"
    for entry in sortedList:
        avg_eco_warrior.add_row(entry)
    str_avg_eco_warrior = avg_eco_warrior.get_string()
    outFile.write(str_avg_eco_warrior)

def printEntryFragger (sortedList, outFile):
    outFile.write('\n\nDaily Entry Fragger: \n')
    entry_fragger = PrettyTable(["Name", "Points", "Opening Kills"])
    entry_fragger.align["Name"] = "l"
    entry_fragger.align["Points"] = "r"
    entry_fragger.align["Opening Kills"] = "r"
    for entry in sortedList:
        entry_fragger.add_row(entry)
    str_entry_fragger = entry_fragger.get_string()
    outFile.write(str_entry_fragger)

def printAvgEntryFragger (sortedList, outFile):
    outFile.write('\n\nEntry Fragger Total Average: \n')
    avg_entry_fragger = PrettyTable(["Name", "Points/Maps", "Opening Kills/Maps", "Maps Played"])
    avg_entry_fragger.float_format = ".2"
    avg_entry_fragger.align = "r"
    avg_entry_fragger.align["Name"] = "l"
    for entry in sortedList:
        avg_entry_fragger.add_row(entry)
    str_avg_entry_fragger = avg_entry_fragger.get_string()
    outFile.write(str_avg_entry_fragger)

def printSniper (sortedList, outFile):
    outFile.write('\n\nDaily Sniper: \n')
    sniper = PrettyTable(["Name", "Points", "Sniper Kills"])
    sniper.align["Name"] = "l"
    sniper.align["Points"] = "r"
    sniper.align["Sniper Kills"] = "r"
    for entry in sortedList:
        sniper.add_row(entry)
    str_sniper = sniper.get_string()
    outFile.write(str_sniper)

def printAvgSniper (sortedList, outFile):
    outFile.write('\n\nSniper Total Average: \n')
    avg_sniper = PrettyTable(["Name", "Points/Maps", "Sniper Kills/Maps", "Maps Played"])
    avg_sniper.float_format = ".2"
    avg_sniper.align = "r"
    avg_sniper.align["Name"] = "l"
    for entry in sortedList:
        avg_sniper.add_row(entry)
    str_avg_sniper = avg_sniper.get_string()
    outFile.write(str_avg_sniper)

with open(input_file) as inputfile:
    data = json.load(inputfile)
    for player in data['items_game_live']['pro_players'].values():
        name = player['name']

        if stageToParse in player['events']['9']:
            thisStage = player['events']['9'][stageToParse]

            kills = int(thisStage['enemy_kills'])
            deaths = int(thisStage['deaths'])
            clutch_kills = int(thisStage['clutch_kills'])
            opening_kills = int(thisStage['opening_kills'])
            pistol_kills = int(thisStage['pistol_kills'])
            sniper_kills = int(thisStage['sniper_kills'])

            calculateDailyCommando(name, kills, deaths)
            calculateDailyClutchKing(name, kills, deaths, clutch_kills)
            calculateDailyEcoWarrior(name, kills, deaths, pistol_kills)
            calculateDailyEntryFragger(name, kills, deaths, opening_kills)
            calculateDailySniper(name, kills, deaths, sniper_kills)

        avg_kills = int(player['events']['9']['enemy_kills'])
        avg_deaths = int(player['events']['9']['deaths'])
        avg_clutch_kills = int(player['events']['9']['clutch_kills'])
        avg_opening_kills = int(player['events']['9']['opening_kills'])
        avg_pistol_kills = int(player['events']['9']['pistol_kills'])
        avg_sniper_kills = int(player['events']['9']['sniper_kills'])
        matches_played = int(player['events']['9']['matches_played'])

        calculateAvgCommando(name, avg_kills, avg_deaths, matches_played)
        calculateAvgClutchKing(name, avg_kills, avg_deaths, avg_clutch_kills, matches_played)
        calculateAvgEcoWarrior(name, avg_kills, avg_deaths, avg_pistol_kills, matches_played)
        calculateAvgEntryFragger(name, avg_kills, avg_deaths, avg_opening_kills, matches_played)
        calculateAvgSniper(name, avg_kills, avg_deaths, avg_sniper_kills, matches_played)

# Daily
s_commando = sorted(commando, key=itemgetter(1, 2), reverse=True)
s_clutch_king = sorted(clutch_king, key=itemgetter(1, 2), reverse=True)
s_eco_warrior = sorted(eco_warrior, key=itemgetter(1, 2), reverse=True)
s_entry_fragger = sorted(entry_fragger, key=itemgetter(1, 2), reverse=True)
s_sniper = sorted(sniper, key=itemgetter(1, 2), reverse=True)

# Total average
s_avg_commando = sorted(avg_commando, key=itemgetter(1, 2, 3), reverse=True)
s_avg_clutch_king = sorted(avg_clutch_king, key=itemgetter(1, 2, 3), reverse=True)
s_avg_eco_warrior = sorted(avg_eco_warrior, key=itemgetter(1, 2, 3), reverse=True)
s_avg_entry_fragger = sorted(avg_entry_fragger, key=itemgetter(1, 2, 3), reverse=True)
s_avg_sniper = sorted(avg_sniper, key=itemgetter(1, 2, 3), reverse=True)

findOptimal()
s_optimal_combinations = sorted(optimal_combinations, key=itemgetter(5), reverse=True)

with open(output_file, 'w') as outFile:
    printOptimal(s_optimal_combinations, outFile)
    printCommando(s_commando, outFile)
    printAvgCommando(s_avg_commando, outFile)
    printClutchKing(s_clutch_king, outFile)
    printAvgClutchKing(s_avg_clutch_king, outFile)
    printEcoWarrior(s_eco_warrior, outFile)
    printAvgEcoWarrior(s_avg_eco_warrior, outFile)
    printEntryFragger(s_entry_fragger, outFile)
    printAvgEntryFragger(s_avg_entry_fragger, outFile)
    printSniper(s_sniper, outFile)
    printAvgSniper(s_avg_sniper, outFile)
