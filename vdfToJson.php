<?php
    //load VDF data either from API call or fetching from file/url
    //no matter your method, $json must contain the VDF data to be parsed

    $input_file = 'C:\Users\Sondre\Google Drive\csgoJsondataParser\final.txt';
    $output_file = 'C:\Users\Sondre\Google Drive\csgoJsondataParser\final.json';

    $json = file_get_contents($input_file);
    //encapsulate in braces
    $json = "{\n$json\n}";
    //replace open braces
    $pattern = '/"([^"]*)"(\s*){/';
    $replace = '"${1}": {';
    $json = preg_replace($pattern, $replace, $json);
    //replace values
    $pattern = '/"([^"]*)"\s*"([^"]*)"/';
    $replace = '"${1}": "${2}",';
    $json = preg_replace($pattern, $replace, $json);
    //remove trailing commas
    $pattern = '/,(\s*[}\]])/';
    $replace = '${1}';
    $json = preg_replace($pattern, $replace, $json);
    //add commas
    $pattern = '/([}\]])(\s*)("[^"]*":\s*)?([{\[])/';
    $replace = '${1},${2}${3}${4}';
    $json = preg_replace($pattern, $replace, $json);
    //object as value
    $pattern = '/}(\s*"[^"]*":)/';
    $replace = '},${1}';
    $json = preg_replace($pattern, $replace, $json);
    //we now have valid json which we can use and/or store it for later use
    file_put_contents($output_file, $json);
?>
